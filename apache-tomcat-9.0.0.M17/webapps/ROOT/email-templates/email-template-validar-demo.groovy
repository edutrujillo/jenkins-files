<!DOCTYPE html>
<html>
<head>
<style>
.button {
  font: bold 11px Arial;
  text-decoration: none;
  background-color: #EEEEEE;
  color: #333333;
  padding: 2px 6px 2px 6px;
  border-top: 1px solid #CCCCCC;
  border-right: 1px solid #333333;
  border-bottom: 1px solid #333333;
  border-left: 1px solid #CCCCCC;
}
</style>
</head>
<body>

${project.name} - Build # ${build.number} - ${build.result}: <br /><br />

Check console output at ${build.url} to view the results. <br /><br />
<% def paramEntorno = "entornoValidar"
def paramRollback= "Rollback"
def resolver = build.buildVariableResolver

def paramEntornoValue = resolver.resolve(paramEntorno)
def paramRollbackValue = resolver.resolve(paramRollback)
%>


<a href="http://40.69.74.208:18080/job/desplegar-paquete-demo/buildWithParameters?delay=0sec&entornoDesplegar=<% println paramEntornoValue %>&Rollback=<%println paramRollbackValue%>&ValidationID=<%
def cmds = [ 'cd /home/jenkins/apache-tomcat-9.0.0.M17/webapps/ROOT/jobs/validar-paquete-demo/builds',
             'log=`find . -name log | xargs ls -ltr | grep -o "\\.[a-z0-9/][a-z0-9/]*" | tail -1`',
             'ValidationId=`cat $log | grep -i "Request ID for the current deploy task" | grep -o [A-Za-z0-9]*[A-Za-z0-9]$`',
             'echo $ValidationId' ]
def result = [ 'bash', '-c', cmds.join(";") ].execute()
println result.text
%>" class="button">Quick Deploy <%println paramRollbackValue%></a>
<br /> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQm9taWRM1Z9pFCKBjlKhIq0vFhB_QzNG2dGU22lIR44K4O7xS44w" alt="Everis" />

</body>
</html>
