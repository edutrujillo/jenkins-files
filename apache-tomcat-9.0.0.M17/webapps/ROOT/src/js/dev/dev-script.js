/*DFRONT: función que crea alt del carrusel de Ideas Corner*/
function createAlt(){
    var title = $(".slick-slide.slick-current.slick-active img").attr("title");
    var alt = $(".slick-slide.slick-current.slick-active img").attr("alt");
    $("[data-function='data-carousel--mobile']").parent().append("<h2 class='tlf-carousel__title'>"+title+"</h2><span class='tlf-carousel__subTitle'>"+alt+"</span>");
};
/*DFRONT: finaliza la función que crea alt del carrusel de Ideas Corner*/
/*DFRONT: función que elimina alt del carrusel de Ideas Corner*/
function deleteAlt(){
    $(".tlf-carousel__title,.tlf-carousel__subTitle").remove();
}
/*DFRONT: finaliza la función que elimina alt del carrusel de Ideas Corner*/
/*DFRONT: función que cambia el alt del carrusel de Ideas Corner*/
function changeAlt(){
    $(".slick-arrow,.slick-dots li").click(function(e){
        e.preventDefault();
        var title = $(".slick-slide.slick-current.slick-active img").attr("title");
        var alt = $(".slick-slide.slick-current.slick-active img").attr("alt");
        $(".tlf-carousel__title").remove();
        $(".data-carousel__title,.tlf-carousel__subTitle").remove();
        $("[data-function='data-carousel--mobile']").parent().append("<h2 class='tlf-carousel__title'>"+title+"</h2><span class='tlf-carousel__subTitle'>"+alt+"</span>");
    });
}
/*DFRONT: finaliza la función que cambia el alt del carrusel de Ideas Corner*/
function openTicket(){
    $("[data-change='data-open-ticket']").on("change",function(){
        $("#tlf-openticket").modal("show");
    })
}

/*DFRONT: funcion que cambia la hora inicio o fin de la modal de relationship management - nueva cita*/
function subirBajarRango(){
    var subir = $("[data-function='data-timeTop']");
    var bajar = $("[data-function='data-timeBottom']");
    var sel   = $('[data-function="data-time"] > option:selected');

    bajar.click(function() {
        var nextElement = $(this).parent().parent().find($('[data-function="data-time"] > option:selected')).next('option');
        if (nextElement.length > 0) {
            $(this).parent().parent().find($('[data-function="data-time"] > option:selected')).removeAttr('selected').next('option').prop('selected', 'selected');
        }
    });

    subir.click(function() {
        var nextElement = $(this).parent().parent().find($('[data-function="data-time"] > option:selected')).prev('option');
        if (nextElement.length > 0) {
            $(this).parent().parent().find($('[data-function="data-time"] > option:selected')).removeAttr('selected').prev('option').prop('selected', 'selected');
        }
    });
}
/*DFRONT: finaliza la funcion que cambia la hora inicio o fin de la modal de relationship management - nueva cita*/

//*DFRONT: función que controla los campos requeridos para activar o desactivar los botones de envio
var require  = (function () {

    // Init, inicializamos la funcion require pasando por parametros el formulario a controlar.
    var init = (function (element) {
        var disabled = false;
        var form = $(element);

        //Creamos un evento change a todos los campos para controlar los cambios en el
        $(element+' [data-required]').on("change",function(){
            //indicamos que todos los campos tienen informacion
            disabled = true;
            //Recorremos todos los campos con el data required, para controlar sus values
            $("[data-form='newcase']").find('[data-required="require"]').each(function(){
                var value = $(this).val();
                //Controlamos que todos los campos tienen valor y ninguno esta vacio o undefined
                if(value !== "undefined" && value !== "" && disabled){
                    disabled = true;
                }else{
                    disabled = false;
                }
            });
            //Si todo es correcto, removemos los estados del boton, si es false lo seguimos manteniendo disabled
            if(disabled){
                form.find('[data-require-btn="btn"]').removeAttr('disabled');
                form.find('[data-require-btn="btn"]').removeClass('tlf-btn--disabled')
            }else{
                form.find('[data-require-btn="btn"]').attr('disabled','disabled');
                form.find('[data-require-btn="btn"]').addClass('tlf-btn--disabled')
            }
        });

        //Control inicial del boton
        if(disabled){
            form.find('[data-require-btn="btn"]').removeAttr('disabled');
            form.find('[data-require-btn="btn"]').removeClass('tlf-btn--disabled')
        }else{
            form.find('[data-require-btn="btn"]').attr('disabled','disabled');
            form.find('[data-require-btn="btn"]').addClass('tlf-btn--disabled')
        }
    });

    return {
        init: init
    };

})();
//*DFRONT: finaliza la funcion de control de campos require

$(document).ready(function () {
    /*DFRONT: se comprueba si existe un select customizado y si es así, lo inicializa*/

    if($('[data-form="newcase"]').length > 0){
        require.init("[data-form='newcase']");
    }


    if($("[data-function='data-selectpicker']").length>0){
        $("[data-function='data-selectpicker']").selectpicker();

    }
    /*DFRONT: finaliza la comprobación del select*/


    if($("[data-function='data-scroller']").length>0){
        $("[data-function='data-scroller']").carousel({
            interval: 3000
        });
    }

    /*DFRONT: se comprueba si existe el carrusel de ideas corner y lo incializa*/
    if($("[data-function='data-carousel']").length>0){
        $("[data-function='data-carousel']").CloudCarousel( {
            reflHeight: 56,
            reflGap:2,
            titleBox: $("[data-function='data-carousel__title']"),
            altBox: $("[data-function='data-carousel__subTitle']"),
            buttonLeft: $("[data-function='data-carousel__buttonLeft']"),
            buttonRight: $("[data-function='data-carousel__buttonRight']"),
            yRadius:40,
            xPos: 331,
            yPos: 33,
            speed:0.15,
            mouseWheel:false,
            carouselRadius:0.5
        });
    }
    /*DFRONT: finaliza la comprobacion del carrusel*/

    /*DFRONT: se comprueba si existe el carrusel de ideas corner en movil y lo inicializa*/
    if($("[data-function='data-carousel--mobile']").length>0){
        if(window.innerWidth>='320' && window.innerWidth<'768'){
            $("[data-function='data-carousel--mobile']").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows:true,
                dots:true,
                autoplay:false,
                draggable:false
            });
            createAlt();
            changeAlt();
        }
        else{
            if ($("[data-function='data-carousel--mobile']").hasClass("slick-initialized")) {
                $("[data-function='data-carousel--mobile']").slick("destroy");
                deleteAlt();
            }
        }
    }
    /*DFRONT: finaliza la comprobacion de si existe el carrusel de ideas corner en movil y lo inicializa*/

    /*DFRONT: se comprueba si existe el carrusel de relationship management equipo y lo inicializa*/
    if($("[data-function='data-carousel__team']").length>0){
        $("[data-function='data-carousel__team']").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            arrows:true,
            dots:false,
            draggable:false,
            centerMode:false,
            speed: 500,
            responsive:[
                {
                    breakpoint:1024,
                    settings:{
                        slidesToShow:2
                    }
                },
                {
                    breakpoint:768,
                    settings:{
                        slidesToShow:1
                    }
                }
            ]
        });
    }
    /*DFRONT: finaliza la comprobacion de si existe el carrusel de relationship management equipo y lo inicializa*/
    if($("[data-function='data-time']").length>0){
        subirBajarRango();
    }

    /*DFRONT: inicializacion de la ordenacion en la tabla*/
    if($("[data-function='data-table']").length>0){
        $("[data-target='#tlf-filter']").on("click",function(e){
            e.stopPropagation();
            $("#tlf-filter").modal('show');
        })
        var theHeaders = {}
        $(this).find('.sorter-false').each(function(i,el){
            theHeaders[$(this).index()] = { sorter: false };
        });
        $("[data-function='data-table']").tablesorter({
            headers: theHeaders
        });


    }
    /*DFRONT: finaliza la inicializacion de la ordenacion en la tabla*/

    /* DFRONT: función para que la modal coja el foco*/
    if($("[data-function='modal-focus']").length>0){
        $("[data-function='modal-focus']").on("shown.bs.modal", function () {
            $("[data-function='focus']").focus();
        });
    }
    /* DFRONT: fin de la función para que la modal coja el foco*/
    /* DFRONT: Inicialización de custom Scrollbar*/
    if($("[data-function='data-scrollbar']").length > 0){
        $("[data-function='data-scrollbar']").mCustomScrollbar(
        {
            scrollButtons:{
                enable:true
            }
        });

    }
    /* DFRONT: Fin de inicialización de custom Scrollbar*/
    /* DFRONT: Función para agregar color al contenedor de los checkboxes*/
    if($("[data-function='data-checkboxes']").length > 0){
        $("[data-function='data-checkboxes-label']").click(function(){
            var checkbox = $(this).prev();
            if (checkbox.is(":checked")){
                $(this).closest("[data-function='data-checkboxes-parent']").removeClass("tlf-modal-checkboxes__row--active");
            }
            else {
                $(this).closest("[data-function='data-checkboxes-parent']").addClass("tlf-modal-checkboxes__row--active");
            }
        });

    }
    /* DFRONT: Fin función para agregar color al contenedor de los checkboxes*/
    if($("[data-change='data-open-ticket']").length>0){
        openTicket();
    }
});

$(window).resize(function(){
    /*DFRONT: funcion que va comprobando si el carrusel de ideascorner existe, se ajusta con el redimensionamiento de la pantalla y según la pantalla lo crea o lo destruye*/
    if($("[data-function='data-carousel--mobile']").length>0){
        if(window.innerWidth>='320' && window.innerWidth<'768'){
            if (!$("[data-function='data-carousel--mobile']").hasClass("slick-initialized")) {
                $("[data-function='data-carousel--mobile']").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows:true,
                    dots:true,
                    autoplay:false
                });
                createAlt();
                changeAlt();
            }
        }
        else {
            if ($("[data-function='data-carousel--mobile']").hasClass("slick-initialized")) {
                $("[data-function='data-carousel--mobile']").slick("destroy");
                deleteAlt();
            }
        }
    }
    /*DFRONT: finaliza la funcion que va comprobando si el carrusel de ideascorner existe, se ajusta con el redimensionamiento de la pantalla y según la pantalla lo crea o lo destruye*/
    /* DFRONT: función para que la modal coja el foco*/
    if($("[data-function='modal-focus']").length>0){
        $("[data-function='modal-focus']").on("shown.bs.modal", function () {
            $("[data-function='focus']").focus();
        });
    }
    /* DFRONT: fin de la función para que la modal coja el foco*/
});

