var dFrontJs = {};

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb3JlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBkRnJvbnRKcyA9IHt9O1xuIl0sImZpbGUiOiJjb3JlLmpzIn0=

/*DFRONT: función que crea alt del carrusel de Ideas Corner*/
function createAlt(){
    var title = $(".slick-slide.slick-current.slick-active img").attr("title");
    var alt = $(".slick-slide.slick-current.slick-active img").attr("alt");
    $("[data-function='data-carousel--mobile']").parent().append("<h2 class='tlf-carousel__title'>"+title+"</h2><span class='tlf-carousel__subTitle'>"+alt+"</span>");
};
/*DFRONT: finaliza la función que crea alt del carrusel de Ideas Corner*/
/*DFRONT: función que elimina alt del carrusel de Ideas Corner*/
function deleteAlt(){
    $(".tlf-carousel__title,.tlf-carousel__subTitle").remove();
}
/*DFRONT: finaliza la función que elimina alt del carrusel de Ideas Corner*/
/*DFRONT: función que cambia el alt del carrusel de Ideas Corner*/
function changeAlt(){
    $(".slick-arrow,.slick-dots li").click(function(e){
        e.preventDefault();
        var title = $(".slick-slide.slick-current.slick-active img").attr("title");
        var alt = $(".slick-slide.slick-current.slick-active img").attr("alt");
        $(".tlf-carousel__title").remove();
        $(".data-carousel__title,.tlf-carousel__subTitle").remove();
        $("[data-function='data-carousel--mobile']").parent().append("<h2 class='tlf-carousel__title'>"+title+"</h2><span class='tlf-carousel__subTitle'>"+alt+"</span>");
    });
}
/*DFRONT: finaliza la función que cambia el alt del carrusel de Ideas Corner*/
function openTicket(){
    $("[data-change='data-open-ticket']").on("change",function(){
        $("#tlf-openticket").modal("show");
    })
}

/*DFRONT: funcion que cambia la hora inicio o fin de la modal de relationship management - nueva cita*/
function subirBajarRango(){
    var subir = $("[data-function='data-timeTop']");
    var bajar = $("[data-function='data-timeBottom']");
    var sel   = $('[data-function="data-time"] > option:selected');

    bajar.click(function() {
        var nextElement = $(this).parent().parent().find($('[data-function="data-time"] > option:selected')).next('option');
        if (nextElement.length > 0) {
            $(this).parent().parent().find($('[data-function="data-time"] > option:selected')).removeAttr('selected').next('option').prop('selected', 'selected');
        }
    });

    subir.click(function() {
        var nextElement = $(this).parent().parent().find($('[data-function="data-time"] > option:selected')).prev('option');
        if (nextElement.length > 0) {
            $(this).parent().parent().find($('[data-function="data-time"] > option:selected')).removeAttr('selected').prev('option').prop('selected', 'selected');
        }
    });
}
/*DFRONT: finaliza la funcion que cambia la hora inicio o fin de la modal de relationship management - nueva cita*/

//*DFRONT: función que controla los campos requeridos para activar o desactivar los botones de envio
var require  = (function () {

    // Init, inicializamos la funcion require pasando por parametros el formulario a controlar.
    var init = (function (element) {
        var disabled = false;
        var form = $(element);

        //Creamos un evento change a todos los campos para controlar los cambios en el
        $(element+' [data-required]').on("change",function(){
            //indicamos que todos los campos tienen informacion
            disabled = true;
            //Recorremos todos los campos con el data required, para controlar sus values
            $("[data-form='newcase']").find('[data-required="require"]').each(function(){
                var value = $(this).val();
                //Controlamos que todos los campos tienen valor y ninguno esta vacio o undefined
                if(value !== "undefined" && value !== "" && disabled){
                    disabled = true;
                }else{
                    disabled = false;
                }
            });
            //Si todo es correcto, removemos los estados del boton, si es false lo seguimos manteniendo disabled
            if(disabled){
                form.find('[data-require-btn="btn"]').removeAttr('disabled');
                form.find('[data-require-btn="btn"]').removeClass('tlf-btn--disabled')
            }else{
                form.find('[data-require-btn="btn"]').attr('disabled','disabled');
                form.find('[data-require-btn="btn"]').addClass('tlf-btn--disabled')
            }
        });

        //Control inicial del boton
        if(disabled){
            form.find('[data-require-btn="btn"]').removeAttr('disabled');
            form.find('[data-require-btn="btn"]').removeClass('tlf-btn--disabled')
        }else{
            form.find('[data-require-btn="btn"]').attr('disabled','disabled');
            form.find('[data-require-btn="btn"]').addClass('tlf-btn--disabled')
        }
    });

    return {
        init: init
    };

})();
//*DFRONT: finaliza la funcion de control de campos require

$(document).ready(function () {
    /*DFRONT: se comprueba si existe un select customizado y si es así, lo inicializa*/

    if($('[data-form="newcase"]').length > 0){
        require.init("[data-form='newcase']");
    }


    if($("[data-function='data-selectpicker']").length>0){
        $("[data-function='data-selectpicker']").selectpicker();

    }
    /*DFRONT: finaliza la comprobación del select*/


    if($("[data-function='data-scroller']").length>0){
        $("[data-function='data-scroller']").carousel({
            interval: 3000
        });
    }

    /*DFRONT: se comprueba si existe el carrusel de ideas corner y lo incializa*/
    if($("[data-function='data-carousel']").length>0){
        $("[data-function='data-carousel']").CloudCarousel( {
            reflHeight: 56,
            reflGap:2,
            titleBox: $("[data-function='data-carousel__title']"),
            altBox: $("[data-function='data-carousel__subTitle']"),
            buttonLeft: $("[data-function='data-carousel__buttonLeft']"),
            buttonRight: $("[data-function='data-carousel__buttonRight']"),
            yRadius:40,
            xPos: 331,
            yPos: 33,
            speed:0.15,
            mouseWheel:false,
            carouselRadius:0.5
        });
    }
    /*DFRONT: finaliza la comprobacion del carrusel*/

    /*DFRONT: se comprueba si existe el carrusel de ideas corner en movil y lo inicializa*/
    if($("[data-function='data-carousel--mobile']").length>0){
        if(window.innerWidth>='320' && window.innerWidth<'768'){
            $("[data-function='data-carousel--mobile']").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows:true,
                dots:true,
                autoplay:false,
                draggable:false
            });
            createAlt();
            changeAlt();
        }
        else{
            if ($("[data-function='data-carousel--mobile']").hasClass("slick-initialized")) {
                $("[data-function='data-carousel--mobile']").slick("destroy");
                deleteAlt();
            }
        }
    }
    /*DFRONT: finaliza la comprobacion de si existe el carrusel de ideas corner en movil y lo inicializa*/

    /*DFRONT: se comprueba si existe el carrusel de relationship management equipo y lo inicializa*/
    if($("[data-function='data-carousel__team']").length>0){
        $("[data-function='data-carousel__team']").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            arrows:true,
            dots:false,
            draggable:false,
            centerMode:false,
            speed: 500,
            responsive:[
                {
                    breakpoint:1024,
                    settings:{
                        slidesToShow:2
                    }
                },
                {
                    breakpoint:768,
                    settings:{
                        slidesToShow:1
                    }
                }
            ]
        });
    }
    /*DFRONT: finaliza la comprobacion de si existe el carrusel de relationship management equipo y lo inicializa*/
    if($("[data-function='data-time']").length>0){
        subirBajarRango();
    }

    /*DFRONT: inicializacion de la ordenacion en la tabla*/
    if($("[data-function='data-table']").length>0){
        $("[data-target='#tlf-filter']").on("click",function(e){
            e.stopPropagation();
            $("#tlf-filter").modal('show');
        })
        var theHeaders = {}
        $(this).find('.sorter-false').each(function(i,el){
            theHeaders[$(this).index()] = { sorter: false };
        });
        $("[data-function='data-table']").tablesorter({
            headers: theHeaders
        });


    }
    /*DFRONT: finaliza la inicializacion de la ordenacion en la tabla*/

    /* DFRONT: función para que la modal coja el foco*/
    if($("[data-function='modal-focus']").length>0){
        $("[data-function='modal-focus']").on("shown.bs.modal", function () {
            $("[data-function='focus']").focus();
        });
    }
    /* DFRONT: fin de la función para que la modal coja el foco*/
    /* DFRONT: Inicialización de custom Scrollbar*/
    if($("[data-function='data-scrollbar']").length > 0){
        $("[data-function='data-scrollbar']").mCustomScrollbar(
        {
            scrollButtons:{
                enable:true
            }
        });

    }
    /* DFRONT: Fin de inicialización de custom Scrollbar*/
    /* DFRONT: Función para agregar color al contenedor de los checkboxes*/
    if($("[data-function='data-checkboxes']").length > 0){
        $("[data-function='data-checkboxes-label']").click(function(){
            var checkbox = $(this).prev();
            if (checkbox.is(":checked")){
                $(this).closest("[data-function='data-checkboxes-parent']").removeClass("tlf-modal-checkboxes__row--active");
            }
            else {
                $(this).closest("[data-function='data-checkboxes-parent']").addClass("tlf-modal-checkboxes__row--active");
            }
        });

    }
    /* DFRONT: Fin función para agregar color al contenedor de los checkboxes*/
    if($("[data-change='data-open-ticket']").length>0){
        openTicket();
    }
});

$(window).resize(function(){
    /*DFRONT: funcion que va comprobando si el carrusel de ideascorner existe, se ajusta con el redimensionamiento de la pantalla y según la pantalla lo crea o lo destruye*/
    if($("[data-function='data-carousel--mobile']").length>0){
        if(window.innerWidth>='320' && window.innerWidth<'768'){
            if (!$("[data-function='data-carousel--mobile']").hasClass("slick-initialized")) {
                $("[data-function='data-carousel--mobile']").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows:true,
                    dots:true,
                    autoplay:false
                });
                createAlt();
                changeAlt();
            }
        }
        else {
            if ($("[data-function='data-carousel--mobile']").hasClass("slick-initialized")) {
                $("[data-function='data-carousel--mobile']").slick("destroy");
                deleteAlt();
            }
        }
    }
    /*DFRONT: finaliza la funcion que va comprobando si el carrusel de ideascorner existe, se ajusta con el redimensionamiento de la pantalla y según la pantalla lo crea o lo destruye*/
    /* DFRONT: función para que la modal coja el foco*/
    if($("[data-function='modal-focus']").length>0){
        $("[data-function='modal-focus']").on("shown.bs.modal", function () {
            $("[data-function='focus']").focus();
        });
    }
    /* DFRONT: fin de la función para que la modal coja el foco*/
});


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJkZXYtc2NyaXB0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qREZST05UOiBmdW5jacOzbiBxdWUgY3JlYSBhbHQgZGVsIGNhcnJ1c2VsIGRlIElkZWFzIENvcm5lciovXG5mdW5jdGlvbiBjcmVhdGVBbHQoKXtcbiAgICB2YXIgdGl0bGUgPSAkKFwiLnNsaWNrLXNsaWRlLnNsaWNrLWN1cnJlbnQuc2xpY2stYWN0aXZlIGltZ1wiKS5hdHRyKFwidGl0bGVcIik7XG4gICAgdmFyIGFsdCA9ICQoXCIuc2xpY2stc2xpZGUuc2xpY2stY3VycmVudC5zbGljay1hY3RpdmUgaW1nXCIpLmF0dHIoXCJhbHRcIik7XG4gICAgJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLWNhcm91c2VsLS1tb2JpbGUnXVwiKS5wYXJlbnQoKS5hcHBlbmQoXCI8aDIgY2xhc3M9J3RsZi1jYXJvdXNlbF9fdGl0bGUnPlwiK3RpdGxlK1wiPC9oMj48c3BhbiBjbGFzcz0ndGxmLWNhcm91c2VsX19zdWJUaXRsZSc+XCIrYWx0K1wiPC9zcGFuPlwiKTtcbn07XG4vKkRGUk9OVDogZmluYWxpemEgbGEgZnVuY2nDs24gcXVlIGNyZWEgYWx0IGRlbCBjYXJydXNlbCBkZSBJZGVhcyBDb3JuZXIqL1xuLypERlJPTlQ6IGZ1bmNpw7NuIHF1ZSBlbGltaW5hIGFsdCBkZWwgY2FycnVzZWwgZGUgSWRlYXMgQ29ybmVyKi9cbmZ1bmN0aW9uIGRlbGV0ZUFsdCgpe1xuICAgICQoXCIudGxmLWNhcm91c2VsX190aXRsZSwudGxmLWNhcm91c2VsX19zdWJUaXRsZVwiKS5yZW1vdmUoKTtcbn1cbi8qREZST05UOiBmaW5hbGl6YSBsYSBmdW5jacOzbiBxdWUgZWxpbWluYSBhbHQgZGVsIGNhcnJ1c2VsIGRlIElkZWFzIENvcm5lciovXG4vKkRGUk9OVDogZnVuY2nDs24gcXVlIGNhbWJpYSBlbCBhbHQgZGVsIGNhcnJ1c2VsIGRlIElkZWFzIENvcm5lciovXG5mdW5jdGlvbiBjaGFuZ2VBbHQoKXtcbiAgICAkKFwiLnNsaWNrLWFycm93LC5zbGljay1kb3RzIGxpXCIpLmNsaWNrKGZ1bmN0aW9uKGUpe1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHZhciB0aXRsZSA9ICQoXCIuc2xpY2stc2xpZGUuc2xpY2stY3VycmVudC5zbGljay1hY3RpdmUgaW1nXCIpLmF0dHIoXCJ0aXRsZVwiKTtcbiAgICAgICAgdmFyIGFsdCA9ICQoXCIuc2xpY2stc2xpZGUuc2xpY2stY3VycmVudC5zbGljay1hY3RpdmUgaW1nXCIpLmF0dHIoXCJhbHRcIik7XG4gICAgICAgICQoXCIudGxmLWNhcm91c2VsX190aXRsZVwiKS5yZW1vdmUoKTtcbiAgICAgICAgJChcIi5kYXRhLWNhcm91c2VsX190aXRsZSwudGxmLWNhcm91c2VsX19zdWJUaXRsZVwiKS5yZW1vdmUoKTtcbiAgICAgICAgJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLWNhcm91c2VsLS1tb2JpbGUnXVwiKS5wYXJlbnQoKS5hcHBlbmQoXCI8aDIgY2xhc3M9J3RsZi1jYXJvdXNlbF9fdGl0bGUnPlwiK3RpdGxlK1wiPC9oMj48c3BhbiBjbGFzcz0ndGxmLWNhcm91c2VsX19zdWJUaXRsZSc+XCIrYWx0K1wiPC9zcGFuPlwiKTtcbiAgICB9KTtcbn1cbi8qREZST05UOiBmaW5hbGl6YSBsYSBmdW5jacOzbiBxdWUgY2FtYmlhIGVsIGFsdCBkZWwgY2FycnVzZWwgZGUgSWRlYXMgQ29ybmVyKi9cbmZ1bmN0aW9uIG9wZW5UaWNrZXQoKXtcbiAgICAkKFwiW2RhdGEtY2hhbmdlPSdkYXRhLW9wZW4tdGlja2V0J11cIikub24oXCJjaGFuZ2VcIixmdW5jdGlvbigpe1xuICAgICAgICAkKFwiI3RsZi1vcGVudGlja2V0XCIpLm1vZGFsKFwic2hvd1wiKTtcbiAgICB9KVxufVxuXG4vKkRGUk9OVDogZnVuY2lvbiBxdWUgY2FtYmlhIGxhIGhvcmEgaW5pY2lvIG8gZmluIGRlIGxhIG1vZGFsIGRlIHJlbGF0aW9uc2hpcCBtYW5hZ2VtZW50IC0gbnVldmEgY2l0YSovXG5mdW5jdGlvbiBzdWJpckJhamFyUmFuZ28oKXtcbiAgICB2YXIgc3ViaXIgPSAkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtdGltZVRvcCddXCIpO1xuICAgIHZhciBiYWphciA9ICQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS10aW1lQm90dG9tJ11cIik7XG4gICAgdmFyIHNlbCAgID0gJCgnW2RhdGEtZnVuY3Rpb249XCJkYXRhLXRpbWVcIl0gPiBvcHRpb246c2VsZWN0ZWQnKTtcblxuICAgIGJhamFyLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgbmV4dEVsZW1lbnQgPSAkKHRoaXMpLnBhcmVudCgpLnBhcmVudCgpLmZpbmQoJCgnW2RhdGEtZnVuY3Rpb249XCJkYXRhLXRpbWVcIl0gPiBvcHRpb246c2VsZWN0ZWQnKSkubmV4dCgnb3B0aW9uJyk7XG4gICAgICAgIGlmIChuZXh0RWxlbWVudC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAkKHRoaXMpLnBhcmVudCgpLnBhcmVudCgpLmZpbmQoJCgnW2RhdGEtZnVuY3Rpb249XCJkYXRhLXRpbWVcIl0gPiBvcHRpb246c2VsZWN0ZWQnKSkucmVtb3ZlQXR0cignc2VsZWN0ZWQnKS5uZXh0KCdvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsICdzZWxlY3RlZCcpO1xuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICBzdWJpci5jbGljayhmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIG5leHRFbGVtZW50ID0gJCh0aGlzKS5wYXJlbnQoKS5wYXJlbnQoKS5maW5kKCQoJ1tkYXRhLWZ1bmN0aW9uPVwiZGF0YS10aW1lXCJdID4gb3B0aW9uOnNlbGVjdGVkJykpLnByZXYoJ29wdGlvbicpO1xuICAgICAgICBpZiAobmV4dEVsZW1lbnQubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgJCh0aGlzKS5wYXJlbnQoKS5wYXJlbnQoKS5maW5kKCQoJ1tkYXRhLWZ1bmN0aW9uPVwiZGF0YS10aW1lXCJdID4gb3B0aW9uOnNlbGVjdGVkJykpLnJlbW92ZUF0dHIoJ3NlbGVjdGVkJykucHJldignb3B0aW9uJykucHJvcCgnc2VsZWN0ZWQnLCAnc2VsZWN0ZWQnKTtcbiAgICAgICAgfVxuICAgIH0pO1xufVxuLypERlJPTlQ6IGZpbmFsaXphIGxhIGZ1bmNpb24gcXVlIGNhbWJpYSBsYSBob3JhIGluaWNpbyBvIGZpbiBkZSBsYSBtb2RhbCBkZSByZWxhdGlvbnNoaXAgbWFuYWdlbWVudCAtIG51ZXZhIGNpdGEqL1xuXG4vLypERlJPTlQ6IGZ1bmNpw7NuIHF1ZSBjb250cm9sYSBsb3MgY2FtcG9zIHJlcXVlcmlkb3MgcGFyYSBhY3RpdmFyIG8gZGVzYWN0aXZhciBsb3MgYm90b25lcyBkZSBlbnZpb1xudmFyIHJlcXVpcmUgID0gKGZ1bmN0aW9uICgpIHtcblxuICAgIC8vIEluaXQsIGluaWNpYWxpemFtb3MgbGEgZnVuY2lvbiByZXF1aXJlIHBhc2FuZG8gcG9yIHBhcmFtZXRyb3MgZWwgZm9ybXVsYXJpbyBhIGNvbnRyb2xhci5cbiAgICB2YXIgaW5pdCA9IChmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICB2YXIgZGlzYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgdmFyIGZvcm0gPSAkKGVsZW1lbnQpO1xuXG4gICAgICAgIC8vQ3JlYW1vcyB1biBldmVudG8gY2hhbmdlIGEgdG9kb3MgbG9zIGNhbXBvcyBwYXJhIGNvbnRyb2xhciBsb3MgY2FtYmlvcyBlbiBlbFxuICAgICAgICAkKGVsZW1lbnQrJyBbZGF0YS1yZXF1aXJlZF0nKS5vbihcImNoYW5nZVwiLGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAvL2luZGljYW1vcyBxdWUgdG9kb3MgbG9zIGNhbXBvcyB0aWVuZW4gaW5mb3JtYWNpb25cbiAgICAgICAgICAgIGRpc2FibGVkID0gdHJ1ZTtcbiAgICAgICAgICAgIC8vUmVjb3JyZW1vcyB0b2RvcyBsb3MgY2FtcG9zIGNvbiBlbCBkYXRhIHJlcXVpcmVkLCBwYXJhIGNvbnRyb2xhciBzdXMgdmFsdWVzXG4gICAgICAgICAgICAkKFwiW2RhdGEtZm9ybT0nbmV3Y2FzZSddXCIpLmZpbmQoJ1tkYXRhLXJlcXVpcmVkPVwicmVxdWlyZVwiXScpLmVhY2goZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICB2YXIgdmFsdWUgPSAkKHRoaXMpLnZhbCgpO1xuICAgICAgICAgICAgICAgIC8vQ29udHJvbGFtb3MgcXVlIHRvZG9zIGxvcyBjYW1wb3MgdGllbmVuIHZhbG9yIHkgbmluZ3VubyBlc3RhIHZhY2lvIG8gdW5kZWZpbmVkXG4gICAgICAgICAgICAgICAgaWYodmFsdWUgIT09IFwidW5kZWZpbmVkXCIgJiYgdmFsdWUgIT09IFwiXCIgJiYgZGlzYWJsZWQpe1xuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAvL1NpIHRvZG8gZXMgY29ycmVjdG8sIHJlbW92ZW1vcyBsb3MgZXN0YWRvcyBkZWwgYm90b24sIHNpIGVzIGZhbHNlIGxvIHNlZ3VpbW9zIG1hbnRlbmllbmRvIGRpc2FibGVkXG4gICAgICAgICAgICBpZihkaXNhYmxlZCl7XG4gICAgICAgICAgICAgICAgZm9ybS5maW5kKCdbZGF0YS1yZXF1aXJlLWJ0bj1cImJ0blwiXScpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICAgICAgZm9ybS5maW5kKCdbZGF0YS1yZXF1aXJlLWJ0bj1cImJ0blwiXScpLnJlbW92ZUNsYXNzKCd0bGYtYnRuLS1kaXNhYmxlZCcpXG4gICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICBmb3JtLmZpbmQoJ1tkYXRhLXJlcXVpcmUtYnRuPVwiYnRuXCJdJykuYXR0cignZGlzYWJsZWQnLCdkaXNhYmxlZCcpO1xuICAgICAgICAgICAgICAgIGZvcm0uZmluZCgnW2RhdGEtcmVxdWlyZS1idG49XCJidG5cIl0nKS5hZGRDbGFzcygndGxmLWJ0bi0tZGlzYWJsZWQnKVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAvL0NvbnRyb2wgaW5pY2lhbCBkZWwgYm90b25cbiAgICAgICAgaWYoZGlzYWJsZWQpe1xuICAgICAgICAgICAgZm9ybS5maW5kKCdbZGF0YS1yZXF1aXJlLWJ0bj1cImJ0blwiXScpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICBmb3JtLmZpbmQoJ1tkYXRhLXJlcXVpcmUtYnRuPVwiYnRuXCJdJykucmVtb3ZlQ2xhc3MoJ3RsZi1idG4tLWRpc2FibGVkJylcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICBmb3JtLmZpbmQoJ1tkYXRhLXJlcXVpcmUtYnRuPVwiYnRuXCJdJykuYXR0cignZGlzYWJsZWQnLCdkaXNhYmxlZCcpO1xuICAgICAgICAgICAgZm9ybS5maW5kKCdbZGF0YS1yZXF1aXJlLWJ0bj1cImJ0blwiXScpLmFkZENsYXNzKCd0bGYtYnRuLS1kaXNhYmxlZCcpXG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIGluaXQ6IGluaXRcbiAgICB9O1xuXG59KSgpO1xuLy8qREZST05UOiBmaW5hbGl6YSBsYSBmdW5jaW9uIGRlIGNvbnRyb2wgZGUgY2FtcG9zIHJlcXVpcmVcblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgIC8qREZST05UOiBzZSBjb21wcnVlYmEgc2kgZXhpc3RlIHVuIHNlbGVjdCBjdXN0b21pemFkbyB5IHNpIGVzIGFzw60sIGxvIGluaWNpYWxpemEqL1xuXG4gICAgaWYoJCgnW2RhdGEtZm9ybT1cIm5ld2Nhc2VcIl0nKS5sZW5ndGggPiAwKXtcbiAgICAgICAgcmVxdWlyZS5pbml0KFwiW2RhdGEtZm9ybT0nbmV3Y2FzZSddXCIpO1xuICAgIH1cblxuXG4gICAgaWYoJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLXNlbGVjdHBpY2tlciddXCIpLmxlbmd0aD4wKXtcbiAgICAgICAgJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLXNlbGVjdHBpY2tlciddXCIpLnNlbGVjdHBpY2tlcigpO1xuXG4gICAgfVxuICAgIC8qREZST05UOiBmaW5hbGl6YSBsYSBjb21wcm9iYWNpw7NuIGRlbCBzZWxlY3QqL1xuXG5cbiAgICBpZigkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtc2Nyb2xsZXInXVwiKS5sZW5ndGg+MCl7XG4gICAgICAgICQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1zY3JvbGxlciddXCIpLmNhcm91c2VsKHtcbiAgICAgICAgICAgIGludGVydmFsOiAzMDAwXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qREZST05UOiBzZSBjb21wcnVlYmEgc2kgZXhpc3RlIGVsIGNhcnJ1c2VsIGRlIGlkZWFzIGNvcm5lciB5IGxvIGluY2lhbGl6YSovXG4gICAgaWYoJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLWNhcm91c2VsJ11cIikubGVuZ3RoPjApe1xuICAgICAgICAkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2Fyb3VzZWwnXVwiKS5DbG91ZENhcm91c2VsKCB7XG4gICAgICAgICAgICByZWZsSGVpZ2h0OiA1NixcbiAgICAgICAgICAgIHJlZmxHYXA6MixcbiAgICAgICAgICAgIHRpdGxlQm94OiAkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2Fyb3VzZWxfX3RpdGxlJ11cIiksXG4gICAgICAgICAgICBhbHRCb3g6ICQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1jYXJvdXNlbF9fc3ViVGl0bGUnXVwiKSxcbiAgICAgICAgICAgIGJ1dHRvbkxlZnQ6ICQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1jYXJvdXNlbF9fYnV0dG9uTGVmdCddXCIpLFxuICAgICAgICAgICAgYnV0dG9uUmlnaHQ6ICQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1jYXJvdXNlbF9fYnV0dG9uUmlnaHQnXVwiKSxcbiAgICAgICAgICAgIHlSYWRpdXM6NDAsXG4gICAgICAgICAgICB4UG9zOiAzMzEsXG4gICAgICAgICAgICB5UG9zOiAzMyxcbiAgICAgICAgICAgIHNwZWVkOjAuMTUsXG4gICAgICAgICAgICBtb3VzZVdoZWVsOmZhbHNlLFxuICAgICAgICAgICAgY2Fyb3VzZWxSYWRpdXM6MC41XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICAvKkRGUk9OVDogZmluYWxpemEgbGEgY29tcHJvYmFjaW9uIGRlbCBjYXJydXNlbCovXG5cbiAgICAvKkRGUk9OVDogc2UgY29tcHJ1ZWJhIHNpIGV4aXN0ZSBlbCBjYXJydXNlbCBkZSBpZGVhcyBjb3JuZXIgZW4gbW92aWwgeSBsbyBpbmljaWFsaXphKi9cbiAgICBpZigkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2Fyb3VzZWwtLW1vYmlsZSddXCIpLmxlbmd0aD4wKXtcbiAgICAgICAgaWYod2luZG93LmlubmVyV2lkdGg+PSczMjAnICYmIHdpbmRvdy5pbm5lcldpZHRoPCc3NjgnKXtcbiAgICAgICAgICAgICQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1jYXJvdXNlbC0tbW9iaWxlJ11cIikuc2xpY2soe1xuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgICAgICAgICAgICBhcnJvd3M6dHJ1ZSxcbiAgICAgICAgICAgICAgICBkb3RzOnRydWUsXG4gICAgICAgICAgICAgICAgYXV0b3BsYXk6ZmFsc2UsXG4gICAgICAgICAgICAgICAgZHJhZ2dhYmxlOmZhbHNlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGNyZWF0ZUFsdCgpO1xuICAgICAgICAgICAgY2hhbmdlQWx0KCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZXtcbiAgICAgICAgICAgIGlmICgkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2Fyb3VzZWwtLW1vYmlsZSddXCIpLmhhc0NsYXNzKFwic2xpY2staW5pdGlhbGl6ZWRcIikpIHtcbiAgICAgICAgICAgICAgICAkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2Fyb3VzZWwtLW1vYmlsZSddXCIpLnNsaWNrKFwiZGVzdHJveVwiKTtcbiAgICAgICAgICAgICAgICBkZWxldGVBbHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICAvKkRGUk9OVDogZmluYWxpemEgbGEgY29tcHJvYmFjaW9uIGRlIHNpIGV4aXN0ZSBlbCBjYXJydXNlbCBkZSBpZGVhcyBjb3JuZXIgZW4gbW92aWwgeSBsbyBpbmljaWFsaXphKi9cblxuICAgIC8qREZST05UOiBzZSBjb21wcnVlYmEgc2kgZXhpc3RlIGVsIGNhcnJ1c2VsIGRlIHJlbGF0aW9uc2hpcCBtYW5hZ2VtZW50IGVxdWlwbyB5IGxvIGluaWNpYWxpemEqL1xuICAgIGlmKCQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1jYXJvdXNlbF9fdGVhbSddXCIpLmxlbmd0aD4wKXtcbiAgICAgICAgJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLWNhcm91c2VsX190ZWFtJ11cIikuc2xpY2soe1xuICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAzLFxuICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgICAgICBhdXRvcGxheTogZmFsc2UsXG4gICAgICAgICAgICBhcnJvd3M6dHJ1ZSxcbiAgICAgICAgICAgIGRvdHM6ZmFsc2UsXG4gICAgICAgICAgICBkcmFnZ2FibGU6ZmFsc2UsXG4gICAgICAgICAgICBjZW50ZXJNb2RlOmZhbHNlLFxuICAgICAgICAgICAgc3BlZWQ6IDUwMCxcbiAgICAgICAgICAgIHJlc3BvbnNpdmU6W1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDoxMDI0LFxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczp7XG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY4LFxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczp7XG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9KTtcbiAgICB9XG4gICAgLypERlJPTlQ6IGZpbmFsaXphIGxhIGNvbXByb2JhY2lvbiBkZSBzaSBleGlzdGUgZWwgY2FycnVzZWwgZGUgcmVsYXRpb25zaGlwIG1hbmFnZW1lbnQgZXF1aXBvIHkgbG8gaW5pY2lhbGl6YSovXG4gICAgaWYoJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLXRpbWUnXVwiKS5sZW5ndGg+MCl7XG4gICAgICAgIHN1YmlyQmFqYXJSYW5nbygpO1xuICAgIH1cblxuICAgIC8qREZST05UOiBpbmljaWFsaXphY2lvbiBkZSBsYSBvcmRlbmFjaW9uIGVuIGxhIHRhYmxhKi9cbiAgICBpZigkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtdGFibGUnXVwiKS5sZW5ndGg+MCl7XG4gICAgICAgICQoXCJbZGF0YS10YXJnZXQ9JyN0bGYtZmlsdGVyJ11cIikub24oXCJjbGlja1wiLGZ1bmN0aW9uKGUpe1xuICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICQoXCIjdGxmLWZpbHRlclwiKS5tb2RhbCgnc2hvdycpO1xuICAgICAgICB9KVxuICAgICAgICB2YXIgdGhlSGVhZGVycyA9IHt9XG4gICAgICAgICQodGhpcykuZmluZCgnLnNvcnRlci1mYWxzZScpLmVhY2goZnVuY3Rpb24oaSxlbCl7XG4gICAgICAgICAgICB0aGVIZWFkZXJzWyQodGhpcykuaW5kZXgoKV0gPSB7IHNvcnRlcjogZmFsc2UgfTtcbiAgICAgICAgfSk7XG4gICAgICAgICQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS10YWJsZSddXCIpLnRhYmxlc29ydGVyKHtcbiAgICAgICAgICAgIGhlYWRlcnM6IHRoZUhlYWRlcnNcbiAgICAgICAgfSk7XG5cblxuICAgIH1cbiAgICAvKkRGUk9OVDogZmluYWxpemEgbGEgaW5pY2lhbGl6YWNpb24gZGUgbGEgb3JkZW5hY2lvbiBlbiBsYSB0YWJsYSovXG5cbiAgICAvKiBERlJPTlQ6IGZ1bmNpw7NuIHBhcmEgcXVlIGxhIG1vZGFsIGNvamEgZWwgZm9jbyovXG4gICAgaWYoJChcIltkYXRhLWZ1bmN0aW9uPSdtb2RhbC1mb2N1cyddXCIpLmxlbmd0aD4wKXtcbiAgICAgICAgJChcIltkYXRhLWZ1bmN0aW9uPSdtb2RhbC1mb2N1cyddXCIpLm9uKFwic2hvd24uYnMubW9kYWxcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJChcIltkYXRhLWZ1bmN0aW9uPSdmb2N1cyddXCIpLmZvY3VzKCk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICAvKiBERlJPTlQ6IGZpbiBkZSBsYSBmdW5jacOzbiBwYXJhIHF1ZSBsYSBtb2RhbCBjb2phIGVsIGZvY28qL1xuICAgIC8qIERGUk9OVDogSW5pY2lhbGl6YWNpw7NuIGRlIGN1c3RvbSBTY3JvbGxiYXIqL1xuICAgIGlmKCQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1zY3JvbGxiYXInXVwiKS5sZW5ndGggPiAwKXtcbiAgICAgICAgJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLXNjcm9sbGJhciddXCIpLm1DdXN0b21TY3JvbGxiYXIoXG4gICAgICAgIHtcbiAgICAgICAgICAgIHNjcm9sbEJ1dHRvbnM6e1xuICAgICAgICAgICAgICAgIGVuYWJsZTp0cnVlXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgfVxuICAgIC8qIERGUk9OVDogRmluIGRlIGluaWNpYWxpemFjacOzbiBkZSBjdXN0b20gU2Nyb2xsYmFyKi9cbiAgICAvKiBERlJPTlQ6IEZ1bmNpw7NuIHBhcmEgYWdyZWdhciBjb2xvciBhbCBjb250ZW5lZG9yIGRlIGxvcyBjaGVja2JveGVzKi9cbiAgICBpZigkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2hlY2tib3hlcyddXCIpLmxlbmd0aCA+IDApe1xuICAgICAgICAkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2hlY2tib3hlcy1sYWJlbCddXCIpLmNsaWNrKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICB2YXIgY2hlY2tib3ggPSAkKHRoaXMpLnByZXYoKTtcbiAgICAgICAgICAgIGlmIChjaGVja2JveC5pcyhcIjpjaGVja2VkXCIpKXtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1jaGVja2JveGVzLXBhcmVudCddXCIpLnJlbW92ZUNsYXNzKFwidGxmLW1vZGFsLWNoZWNrYm94ZXNfX3Jvdy0tYWN0aXZlXCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5jbG9zZXN0KFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2hlY2tib3hlcy1wYXJlbnQnXVwiKS5hZGRDbGFzcyhcInRsZi1tb2RhbC1jaGVja2JveGVzX19yb3ctLWFjdGl2ZVwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICB9XG4gICAgLyogREZST05UOiBGaW4gZnVuY2nDs24gcGFyYSBhZ3JlZ2FyIGNvbG9yIGFsIGNvbnRlbmVkb3IgZGUgbG9zIGNoZWNrYm94ZXMqL1xuICAgIGlmKCQoXCJbZGF0YS1jaGFuZ2U9J2RhdGEtb3Blbi10aWNrZXQnXVwiKS5sZW5ndGg+MCl7XG4gICAgICAgIG9wZW5UaWNrZXQoKTtcbiAgICB9XG59KTtcblxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe1xuICAgIC8qREZST05UOiBmdW5jaW9uIHF1ZSB2YSBjb21wcm9iYW5kbyBzaSBlbCBjYXJydXNlbCBkZSBpZGVhc2Nvcm5lciBleGlzdGUsIHNlIGFqdXN0YSBjb24gZWwgcmVkaW1lbnNpb25hbWllbnRvIGRlIGxhIHBhbnRhbGxhIHkgc2Vnw7puIGxhIHBhbnRhbGxhIGxvIGNyZWEgbyBsbyBkZXN0cnV5ZSovXG4gICAgaWYoJChcIltkYXRhLWZ1bmN0aW9uPSdkYXRhLWNhcm91c2VsLS1tb2JpbGUnXVwiKS5sZW5ndGg+MCl7XG4gICAgICAgIGlmKHdpbmRvdy5pbm5lcldpZHRoPj0nMzIwJyAmJiB3aW5kb3cuaW5uZXJXaWR0aDwnNzY4Jyl7XG4gICAgICAgICAgICBpZiAoISQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1jYXJvdXNlbC0tbW9iaWxlJ11cIikuaGFzQ2xhc3MoXCJzbGljay1pbml0aWFsaXplZFwiKSkge1xuICAgICAgICAgICAgICAgICQoXCJbZGF0YS1mdW5jdGlvbj0nZGF0YS1jYXJvdXNlbC0tbW9iaWxlJ11cIikuc2xpY2soe1xuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgICAgICAgICAgICAgICBhdXRvcGxheTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgYXJyb3dzOnRydWUsXG4gICAgICAgICAgICAgICAgICAgIGRvdHM6dHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgYXV0b3BsYXk6ZmFsc2VcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBjcmVhdGVBbHQoKTtcbiAgICAgICAgICAgICAgICBjaGFuZ2VBbHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGlmICgkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2Fyb3VzZWwtLW1vYmlsZSddXCIpLmhhc0NsYXNzKFwic2xpY2staW5pdGlhbGl6ZWRcIikpIHtcbiAgICAgICAgICAgICAgICAkKFwiW2RhdGEtZnVuY3Rpb249J2RhdGEtY2Fyb3VzZWwtLW1vYmlsZSddXCIpLnNsaWNrKFwiZGVzdHJveVwiKTtcbiAgICAgICAgICAgICAgICBkZWxldGVBbHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICAvKkRGUk9OVDogZmluYWxpemEgbGEgZnVuY2lvbiBxdWUgdmEgY29tcHJvYmFuZG8gc2kgZWwgY2FycnVzZWwgZGUgaWRlYXNjb3JuZXIgZXhpc3RlLCBzZSBhanVzdGEgY29uIGVsIHJlZGltZW5zaW9uYW1pZW50byBkZSBsYSBwYW50YWxsYSB5IHNlZ8O6biBsYSBwYW50YWxsYSBsbyBjcmVhIG8gbG8gZGVzdHJ1eWUqL1xuICAgIC8qIERGUk9OVDogZnVuY2nDs24gcGFyYSBxdWUgbGEgbW9kYWwgY29qYSBlbCBmb2NvKi9cbiAgICBpZigkKFwiW2RhdGEtZnVuY3Rpb249J21vZGFsLWZvY3VzJ11cIikubGVuZ3RoPjApe1xuICAgICAgICAkKFwiW2RhdGEtZnVuY3Rpb249J21vZGFsLWZvY3VzJ11cIikub24oXCJzaG93bi5icy5tb2RhbFwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKFwiW2RhdGEtZnVuY3Rpb249J2ZvY3VzJ11cIikuZm9jdXMoKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIC8qIERGUk9OVDogZmluIGRlIGxhIGZ1bmNpw7NuIHBhcmEgcXVlIGxhIG1vZGFsIGNvamEgZWwgZm9jbyovXG59KTtcblxuIl0sImZpbGUiOiJkZXYtc2NyaXB0LmpzIn0=



//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuIl0sImZpbGUiOiJtYWluLmpzIn0=
